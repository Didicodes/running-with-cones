.. _glossary:

############
  Glossary
############

.. glossary::

    VLC
        VLC Media Player, the "cone that plays anything". Check its `website <https://www.videolan.org/vlc/>`_.

