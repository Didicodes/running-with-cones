.. Running with cones documentation master file, created by
   sphinx-quickstart on Sun May 12 21:02:09 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: macros.rst

.. toctree::
   :hidden:
   :maxdepth: 2
   :glob:

   guides/index
   other/index
   glossary

Welcome to Running with cones!
==============================

A place where you may find *alternative* ways of using |VLC|.

Sections
========

.. The image ratio is: width: 350px; height: 350/4 + (2x5) ~= 98px

.. container:: tocdescr

   .. container:: descr

      .. figure:: /images/guides.jpg
                     :target: guides/index.html

      :doc:`/guides/index`
         Some guides around VLC

   .. container:: descr

      .. figure:: /images/other.jpg
                     :target: other/index.html

      :doc:`/other/index`
         Other stuff.

   .. container:: descr

      :doc:`Glossary </glossary>`
         A list of terms and definitions used in VLC and this place.

   .. container:: descr

      :ref:`Manual Index <genindex>`
         A list of terms linked to the Glossary.


